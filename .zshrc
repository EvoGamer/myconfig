# Ver:	160327.1

####
#Global OPTS
####

setopt autocd autopushd cdablevars pushdsilent pushdtohome autolist autonamedirs
setopt autoparamkeys autoparamslash listambiguous listrowsfirst listtypes menucomplete
setopt recexact cshnullglob extendedglob globdots appendhistory extendedhistory
setopt histexpiredupsfirst histfindnodups histreduceblanks incappendhistory sharehistory
setopt allexport clobber correct interactivecomments hashcmds hashdirs rcquotes shortloops
setopt autoresume checkjobs longlistjobs notify promptsubst transientrprompt nobeep

####
# Modules to Autoload
####

zmodload -a zsh/stat stat
zmodload -a zsh/zpty zpty
zmodload -a zsh/zprof zprof
zmodload -ap zsh/mapfile mapfile

autoload colors zsh/terminfo zkbd
autoload -U compinit

####
# User Vars
####

TZ="Europe/London"
HISTFILE=$HOME/.zshistory
HISTSIZE=10000
SAVEHIST=1000
HOSTNAME="`hostname`"
PAGER='less'
EDITOR='nano'

source /etc/machine-info

####
# Magic Functions
####

function precmd ()
{
	(( TERMWIDTH = ${COLUMNS} - 1 ))
	
	###
	# Truncate the path if it's too long.
	
	PR_FILLBAR=""
	PR_PWDLEN=""
	
	local promptsize=${#${(%):---(%n@$PRETTY_HOSTNAME)---()--}}
	local pwdsize=${#${(%):-%~}}
	
	if [[ "$promptsize + $pwdsize" -gt $TERMWIDTH ]]; then
		((PR_PWDLEN=$TERMWIDTH - $promptsize))
	else
		PR_FILLBAR="\${(l.(($TERMWIDTH - ($promptsize + $pwdsize)))..${PR_HBAR}.)}"
	fi
}

function preexec ()
{
	if [[ "$TERM" == "tmux" ]]; then
		local CMD=${1[(wr)^(*=*|sudo|-*)]}
		echo -ne "\ek$CMD\e\\"
	fi
}

function zshexit ()
{
	if [ "$PRETTY_HOSTNAME" != "AlarmPi" ]; then
		rcupdate
	fi
}

####
# Normal Functions
####

function rcupdate()
{
	cd ~/.myconfig
	git pull
	cp .zshrc ~/
}

function prep_kbd()
{
	function zkbd_file()
	{
		[[ -f ~/.zkbd/${TERM}-${VENDOR}-${OSTYPE} ]] && printf '%s' ~/".zkbd/${TERM}-${VENDOR}-${OSTYPE}" && return 0
		[[ -f ~/.zkbd/${TERM}-${DISPLAY}          ]] && printf '%s' ~/".zkbd/${TERM}-${DISPLAY}"          && return 0
		return 1
	}

	[[ ! -d ~/.zkbd ]] && mkdir ~/.zkbd
	keyfile=$(zkbd_file)
	ret=$?
	if [[ ${ret} -ne 0 ]]; then
		zkbd
		keyfile=$(zkbd_file)
	ret=$?
	fi
	if [[ ${ret} -eq 0 ]] ; then
		source "${keyfile}"
	else
	printf 'Failed to setup keys using zkbd.\n'
	fi
}

function prep_promptcolors()
{
	if [[ "$terminfo[colors]" -ge 8 ]]; then
		colors
	fi
	for color in RED GREEN YELLOW BLUE MAGENTA CYAN WHITE; do
		eval PR_$color='%{$terminfo[bold]$fg[${(L)color}]%}'
		eval PR_LIGHT_$color='%{$fg[${(L)color}]%}'
		(( count = $count + 1 ))
	done
	PR_NO_COLOR="%{$terminfo[sgr0]%}"
}

function prep_prompt()
{
	if [[ "`locale charmap`" = "UTF-8" ]]; then
		PR_SET_CHARSET=""
		PR_SHIFT_IN=""
		PR_SHIFT_OUT=""
		PR_HBAR=$'\u2500'
		PR_ULCORNER=$'\xE2\x94\x8C'
		PR_LLCORNER=$'\xE2\x94\x94'
		PR_LRCORNER=$'\xE2\x94\x98'
		PR_URCORNER=$'\xE2\x94\x90'
	else
		typeset -A altchar
		set -A altchar ${(s..)terminfo[acsc]}
		PR_SET_CHARSET="%{$terminfo[enacs]%}"
		PR_SHIFT_IN="%{$terminfo[smacs]%}"
		PR_SHIFT_OUT="%{$terminfo[rmacs]%}"
		PR_HBAR=${altchar[q]:--}
		PR_ULCORNER=${altchar[l]:--}
		PR_LLCORNER=${altchar[m]:--}
		PR_LRCORNER=${altchar[j]:--}
		PR_URCORNER=${altchar[k]:--}
	fi
}

####
# Prompts
####
prep_promptcolors
prep_prompt

case $TERM in
	xterm*)
		PR_TITLEBAR=$'%{\e]0;%(!.-=*[ROOT]*=- | .)%n@$PRETTY_HOSTNAME:%~ | ${COLUMNS}x${LINES} | %y\a%}';;
	tmux)
		PR_TITLEBAR=$'%{\e_screen \005 (\005t) | %(!.-=[ROOT]=- | .)%n@$PRETTY_HOSTNAME:%~ | ${COLUMNS}x${LINES} | %y\e\\%}';;
	*)
		PR_TITLEBAR='';;
esac

PROMPT='$PR_SET_CHARSET$PR_STITLE${(e)PR_TITLEBAR}\
$PR_CYAN$PR_SHIFT_IN$PR_ULCORNER$PR_BLUE$PR_HBAR$PR_SHIFT_OUT(\
%(!.$PR_RED%SROOT%s.$PR_GREEN%n)$PR_GREEN@$PRETTY_HOSTNAME\
$PR_BLUE)$PR_SHIFT_IN$PR_HBAR$PR_CYAN$PR_HBAR${(e)PR_FILLBAR}$PR_BLUE$PR_HBAR$PR_SHIFT_OUT(\
$PR_MAGENTA%$PR_PWDLEN<...<%~%<<\
$PR_BLUE)$PR_SHIFT_IN$PR_HBAR$PR_CYAN$PR_URCORNER$PR_SHIFT_OUT\

$PR_CYAN$PR_SHIFT_IN$PR_LLCORNER$PR_BLUE$PR_HBAR$PR_SHIFT_OUT(\
$PR_YELLOW%D{%H:%M}\
$PR_LIGHT_BLUE:%(!.$PR_RED#.$PR_WHITE$)$PR_BLUE)$PR_SHIFT_IN$PR_HBAR$PR_SHIFT_OUT\
$PR_CYAN$PR_SHIFT_IN$PR_HBAR$PR_SHIFT_OUT\
$PR_NO_COLOR '

RPROMPT=' $PR_CYAN$PR_SHIFT_IN$PR_HBAR$PR_BLUE$PR_HBAR$PR_SHIFT_OUT\
($PR_YELLOW%D{%a, %d-%m-%Y %H:%M:%S}$PR_BLUE)$PR_SHIFT_IN$PR_HBAR$PR_CYAN$PR_LRCORNER$PR_SHIFT_OUT$PR_NO_COLOR'

#PS1="[$PR_WHITE%!$PR_NO_COLOR][$PR_BLUE%n$PR_NO_COLOR:$PR_RED%2~$PR_NO_COLOR] $PR_WHITE%(!.#.$) "
#RPS1="$PR_LIGHT_YELLOW(%D{%a, %d-%m-%Y %H:%M:%S})$PR_WHITE"

####
# Misc
####

alias ls='ls --color -lAh'

#alias ssh='eval $(/usr/bin/keychain --eval --agents ssh -Q --quiet ~/.ssh/id_rsa) && ssh' keychain 
#~/.ssh/id_rsa . ~/.keychain/${HOSTNAME}-sh

export XDG_DATA_DIRS="/opt/kde/share:$XDG_DATA_DIRS"

prep_kbd
[[ -n "${key[Home]}"    ]]  && bindkey  "${key[Home]}"    beginning-of-line
[[ -n "${key[End]}"     ]]  && bindkey  "${key[End]}"     end-of-line
[[ -n "${key[Insert]}"  ]]  && bindkey  "${key[Insert]}"  overwrite-mode
[[ -n "${key[Delete]}"  ]]  && bindkey  "${key[Delete]}"  delete-char
[[ -n "${key[Up]}"      ]]  && bindkey  "${key[Up]}"      up-line-or-history
[[ -n "${key[Down]}"    ]]  && bindkey  "${key[Down]}"    down-line-or-history
[[ -n "${key[Left]}"    ]]  && bindkey  "${key[Left]}"    backward-char
[[ -n "${key[Right]}"   ]]  && bindkey  "${key[Right]}"   forward-char

compinit
